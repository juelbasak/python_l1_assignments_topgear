import random

def avg(sample_list):
    return sum(sample_list) / len(sample_list)

list1 = [random.randint(0,100) for i in range(10)]
list2 = [random.randint(0,100) for i in range(10)]
list3 = [random.randint(0,100) for i in range(10)]

print('list1 -> ',list1)
print('list2-> ',list2)
print('list3 -> ',list3)

maxlist = sorted(list1)[-2:] + sorted(list2)[-2:] + sorted(list3)[-2:]
minlist = sorted(list1)[:2] + sorted(list2)[:2] + sorted(list3)[:2]

print('Maxlist -> ', maxlist)
print('Average Value of maxlist -> ', avg(maxlist))

print('Minlist -> ', minlist)
print('Average Value of minlist -> ', avg(minlist))


