# No data provided hence, I assumed a sample text and removed all duplicate elements

text = """Dreamsnake is a 1978 science fiction novel by American writer Vonda N. McIntyre. It is an expansion of her Nebula Award–winning 1973 novelette "Of Mist, and Grass, and Sand". The main character, Snake, is a healer who uses genetically modified serpents to cure sickness in the aftermath of a nuclear holocaust on Earth. The titular "dreamsnake" is an alien serpent whose venom gives dying people pleasant dreams. The novel follows Snake as she seeks to replace her dead dreamsnake. The book is an example of second-wave feminism in science fiction. McIntyre subverted gendered narratives, including by placing a woman at the center of a heroic quest. Dreamsnake also explored social structures and sexuality from a feminist perspective, and examined themes of healing and cross-cultural interaction. The novel won the 1978 Nebula Award, the 1979 Hugo Award, and the 1979 Locus Poll Award. Snake's strength and self-sufficiency were noted by commentators, while McIntyre's writing and the book's themes also received praise."""


text_list = text.split(' ')
no_dupli = set(text_list)

print(no_dupli)

