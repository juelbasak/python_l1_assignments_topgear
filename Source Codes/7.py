def power(bits):
    c = 0
    get_sum = 0
    for i in bits[::-1]:
        if i == '1':
            get_sum += 2 ** c
        c+=1

    return get_sum

n = int(input('Prefix/Net Mask -> '))
if n > 32:
    print('Invalid Input. Should be between (0-32)')
    exit()

zeros = 32 - n
data = '1' * n + '0' * zeros + ' '

# print(data)

mylist = []
w = ''
output = ''


for i in range(len(data)):
    if i % 8 == 0 and i != 0:

        output += str(power(w)) + '.'
        # mylist.append(w)
        w = ''

    w += data[i]

output = output[:-1]
print(output)