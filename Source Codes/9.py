import os

base = 'sample_files/'
f_list = []
for _,_,files in os.walk(base):
    f_list = files[:]

# print(f_list)

mydict = {}
for i in f_list:
    mydict[i] = os.path.getsize(base + i)
    print(f'File name -> {i} \t Size -> {mydict[i]}')

# print(mydict)
count = 0
files = []
for key, value in mydict.items():
    if value == 0:
        files.append(key)
        count += 1

if count == 0:
    print('No file with 0 bytes')
else:
    print(f'Found {count} files -> {files}')
        
