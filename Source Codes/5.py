# Python3.8

try:
    with open('input.txt', 'r') as fptr:
        for line in fptr:
            print(line)
            line += ' '
            num_of_char = 0
            num_of_words = 0
            for char in line:
                if char != ' ':
                    num_of_char += 1
                else:
                    num_of_words += 1
            
            
            with open('output.txt', 'a+') as writer:
                writer.write(f'Number of Character -> {num_of_char}')
                writer.write(f'\tNumber of Word -> {num_of_words}\n')

except FileNotFoundError:
    print('File is not Present')
